= 在应用程序中安装 Turbo
:description: Learn how to install Turbo in your application.
:permalink: /handbook/installing.html

Turbo can either be installed in compiled form by referencing the Turbo distributable script directly in the `<head>` of your application or through npm via a bundler like Webpack.

== In Compiled Form

You can download the latest distributable script from the GitHub releases page, then reference that in your `<script>` tag on your page. Or you can float on the latest release of Turbo using a CDN bundler like Skypack. See https://cdn.skypack.dev/@hotwired/turbo for more details.

== As An npm Package

You can install Turbo from npm via the `npm` or `yarn` packaging tools. Then require or import that in your code:

[,javascript]
----
import * as Turbo from "@hotwired/turbo"
----

== 在一个 Ruby on Rails 应用程序中

Turbo JavaScript 框架已经用 https://github.com/hotwired/turbo-rails[turbo-rails Gem] 打包直接用于 Asset Pipeline 了。
